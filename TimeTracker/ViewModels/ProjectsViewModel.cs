﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TimeTracker.Models;

namespace TimeTracker.ViewModels
{
    public class ProjectsCreateViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        [Display(Name = "Client")]
        public int ClientID { get; set; }

        public List<Client> Clients { get; set; }
    }
}