﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTracker.Models;

namespace TimeTracker.ViewModels
{
    public class DashboardData
    {
        public List<Project> Projects;
        public List<TaskItem> Tasks;
        public decimal TaskHours;
        public int ProjectsCount;
        public int ClientsCount;
    }
}