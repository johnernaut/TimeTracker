﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TimeTracker.Models;

namespace TimeTracker.ViewModels
{
    public class TasksCreateViewModel
    {
        [Required, MaxLength(length:50)]
        public string Description { get; set; }
        [Required, MaxLength(length:500), DataType(DataType.MultilineText)]
        public string Body { get; set; }
        [Required]
        public decimal Hours { get; set; }
        [Required]
        public DateTime TaskDate { get; set; }
        [Required, Display(Name = "Project")]
        public int ProjectId { get; set; }
        [Required, Display(Name = "Client")]
        public int ClientId { get; set; }

        public List<Client> Clients { get; set; }
    }
}