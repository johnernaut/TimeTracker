namespace TimeTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datetime_nullable_tasks : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaskItems", "TaskDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskItems", "TaskDate", c => c.DateTime(nullable: false));
        }
    }
}
