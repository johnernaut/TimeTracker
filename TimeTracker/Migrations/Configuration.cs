namespace TimeTracker.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TimeTracker.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TimeTracker.Models.ApplicationDbContext context)
        {
            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Admin" };

                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "john@johnjohnson.cc"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser
                {
                    UserName = "john@johnjohnson.cc",
                    Email = "john@johnjohnson.cc"
                };

                manager.Create(user, "Password1!");
                manager.AddToRole(user.Id, "Admin");
            }

            if (!context.Clients.Any(c => c.Name == "Anadarko"))
            {
                var user = context.Users.Where(u => u.UserName == "john@johnjohnson.cc").First();
                var clientNames = new List<string>() { "Anadarko", "Corinthian", "Microsoft", "Structural Steel" };
                var projectNames = new List<string>() { "Parts Website", "WCF Server", "Email Polling Service", "Ion Cannon", "iPhone 12 App", "Syspro Plugin", "SQL Script" };
                var taskNames = new List<string>() { "Generate project", "Setup SQL script", "Prepare data for the app store",
                    "Add in authentication", "Set up custom registration", "Pull out Syspro XML",
                    "Start work on WCF service", "Create REST API endpoint", "Create custom email server for client" };
                var random = new Random();
                var clients = new List<Client>();
                var projects = new List<Project>();

                for (int i = 0; i < clientNames.Count; i++)
                {
                    var client = new Client() { Name = clientNames[i] };
                    client.Users.Add(user);
                    clients.Add(client);
                    context.Clients.Add(client);
                }

                for (int z = 0; z < clientNames.Count; z++)
                {
                    for (int i = 0; i < projectNames.Count; i++)
                    {
                        var proj = new Project()
                        {
                            Name = projectNames[i],
                            Status = "Started"
                        };

                        proj.Client = clients[z];
                        proj.Users.Add(user);
                        projects.Add(proj);
                        context.Projects.Add(proj);
                    }
                }

                for (int i = 0; i < projects.Count; i++)
                {
                    for (int z = 0; z < taskNames.Count; z++)
                    {
                        var tsk = new TaskItem()
                        {
                            Description = taskNames[z],
                            Body = "Lorem ipsum dolor sit amet shinavi calem bitat.  Lorem ipsum dolor sit amet shinavi calem bitat lorem ipsum dolor sit amet shinavi calem bitat.",
                            Hours = random.Next(0, 10),
                            User = user,
                            TaskDate = DateTime.Now,
                            Project = projects[i]
                        };

                        context.Tasks.Add(tsk);
                    }
                }

                try
                {
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
    }
}
