﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TimeTracker.Models
{
    public class Project : BaseEntity
    {  
        public Project()
        {
            Users = new HashSet<ApplicationUser>();
            Tasks = new HashSet<TaskItem>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public int ClientId { get; set; }

        [NotMapped]
        public static List<string> STATUS_TYPES
        {
            get
            {
                return new List<string>() { "Started", "Finished" };
            }
        }

        public virtual ICollection<ApplicationUser> Users { get; set; }
        public virtual ICollection<TaskItem> Tasks { get; set; }
        public virtual Client Client { get; set; }
    }
}