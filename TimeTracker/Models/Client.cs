﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTracker.Models
{
    public class Client : BaseEntity
    {
        public Client()
        {
            Users = new HashSet<ApplicationUser>();
            Projects = new HashSet<Project>();
        }

        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}