﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTracker.Models
{
    public class TaskItem : BaseEntity
    {
        public int ID { get; set; }
        public decimal Hours { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public DateTime? TaskDate { get; set; }

        public int ProjectId { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual Project Project { get; set; }
    }
}