﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TimeTracker.ViewModels;

namespace TimeTracker.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private ApplicationUserManager _userManager;

        public DashboardController()
        {
        }

        public DashboardController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        // GET: Dashboard
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId();
            var user = await UserManager.FindByIdAsync(userId);
            var projects = user.Projects.Where(p => p.Status == "Started");
            var model = new DashboardData()
            {
                ProjectsCount = projects.Count(),
                Projects = projects.ToList(),
                Tasks = projects.SelectMany(p => p.Tasks).ToList(),
                ClientsCount = user.Clients.Count(),
                TaskHours = projects.SelectMany(p => p.Tasks).Sum(t => t.Hours)
            };

            return View(model);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}