﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeTracker.Models;
using PagedList;
using TimeTracker.ViewModels;
using Microsoft.AspNet.Identity;

namespace TimeTracker.Controllers
{
    public class TasksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tasks
        public async Task<ActionResult> Index(int? page)
        {
            int pageSize = 15;
            int pageNumber = (page ?? 1);
            var tasks = db.Tasks.Include(t => t.Project).Include(t => t.User).
                OrderByDescending(p => p.CreatedAt).ToPagedList(pageNumber, pageSize);
            return View(tasks);
        }

        public ActionResult GetProjectByClientId(int clientId)
        {
            var projects = db.Projects.Where(p => p.ClientId == clientId).ToList();
            var selList = new SelectList(projects, "ID", "Name", 0);
            return Json(selList);
        }

        // GET: Tasks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TaskItem taskItem = await db.Tasks.FindAsync(id);
            if (taskItem == null)
            {
                return HttpNotFound();
            }
            return View(taskItem);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            var clients = db.Clients.ToList();
            var model = new TasksCreateViewModel()
            {
                Clients = clients
            };

            return View(model);
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Hours,Description,Body,CreatedAt,UpdatedAt,ProjectId,TaskDate")] TasksCreateViewModel taskItem)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.Find(User.Identity.GetUserId());
                var task = new TaskItem()
                {
                    Description = taskItem.Description,
                    ProjectId = taskItem.ProjectId,
                    Body = taskItem.Body,
                    User = user,
                    Hours = taskItem.Hours,
                    TaskDate = taskItem.TaskDate
                };
                
                db.Tasks.Add(task);
                await db.SaveChangesAsync();

                return RedirectToAction("index");
            }

            return View(taskItem);
        }

        // GET: Tasks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TaskItem taskItem = await db.Tasks.FindAsync(id);
            if (taskItem == null)
            {
                return HttpNotFound();
            }
            return View(taskItem);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Hours,Description,Body,CreatedAt,UpdatedAt")] TasksCreateViewModel taskItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(taskItem).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(taskItem);
        }

        // GET: Tasks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TaskItem taskItem = await db.Tasks.FindAsync(id);
            if (taskItem == null)
            {
                return HttpNotFound();
            }
            return View(taskItem);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TaskItem taskItem = await db.Tasks.FindAsync(id);
            db.Tasks.Remove(taskItem);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
